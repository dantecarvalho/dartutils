import 'dart:convert';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:intl/intl.dart';
import 'package:dart_date/dart_date.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

String hashMd5(String value) {
  var encode = utf8.encode(value);
  var convert = sha512.convert(encode);
  return convert.toString();
}

String decode64(String string) {
  var base64decode = base64Decode(string);
  return utf8.decode(base64decode);
}

String _password = "gv3049jktg09rg";

String encryptString(String plainText) {
  final key = generateKeyFromPassword(_password);
  final iv = IV.fromSecureRandom(16);
  final encrypter = Encrypter(AES(key));
  final encrypted = encrypter.encrypt(plainText, iv: iv);
  final combined = iv.bytes + encrypted.bytes;
  return base64.encode(combined);
}

String decryptString(String encryptedText) {
  final key = generateKeyFromPassword(_password);
  final encryptedBytes = base64.decode(encryptedText);
  final iv = IV(Uint8List.fromList(encryptedBytes.sublist(0, 16)));
  final ciphertext = Encrypted(Uint8List.fromList(encryptedBytes.sublist(16)));
  final encrypter = Encrypter(AES(key));
  final decrypted = encrypter.decrypt(ciphertext, iv: iv);
  return decrypted;
}

encrypt.Key generateKeyFromPassword(String password) {
  final keyBytes = sha256.convert(utf8.encode(password)).bytes;
  return encrypt.Key(Uint8List.fromList(keyBytes));
}

String aQuantoTempo(DateTime inicio) {
  var fim = DateTime.now();

  var difference = fim.difference(inicio);

  int minutes = difference.inMinutes;
  int hours = difference.inHours;
  int days = difference.inDays;
  int months = days ~/ 30;
  int years = months ~/ 12;

  if (hours == 0) {
    return "Há $minutes ${minutes == 1 ? "minuto" : "minutos"}";
  } else if (days == 0) {
    int minutes2 = fim.subHours(hours).differenceInMinutes(inicio);
    return "Há $hours ${hours == 1 ? "hora" : "horas"}, $minutes2 ${minutes2 == 1 ? "minuto" : "minutos"}";
  } else if (months == 0) {
    int hours2 = fim.subDays(days).differenceInHours(inicio);
    return "Há $days ${days == 1 ? "dia" : "dias"}, $hours2 ${hours2 == 1 ? "hora" : "horas"}";
  } else if (years == 0) {
    int days2 = fim.subMonths(months).differenceInDays(inicio);
    return "Há $months ${months == 1 ? "mês" : "meses"}, $days2 ${days2 == 1 ? "dia" : "dias"}";
  } else {
    int months2 = fim.subYears(years).difference(inicio).inDays ~/ 30;
    return "Há $years ${years == 1 ? "ano" : "anos"}, $months2 ${months2 == 1 ? "mẽs" : "meses"}";
  }
}

String tempoEntre(DateTime inicio, DateTime fim) {
  var difference = fim.difference(inicio);

  int minutes = difference.inMinutes;
  int hours = difference.inHours;
  int days = difference.inDays;
  int months = days ~/ 30;
  int years = months ~/ 12;
  if (hours == 0) {
    return "$minutes ${minutes == 1 ? "minuto" : "minutos"}";
  } else if (days == 0) {
    int minutes2 = fim.subHours(hours).differenceInMinutes(inicio);
    return "$hours ${hours == 1 ? "hora" : "horas"}, $minutes2 ${minutes2 == 1 ? "minuto" : "minutos"}";
  } else if (months == 0) {
    int hours2 = fim.subDays(days).differenceInHours(inicio);
    return "$days ${days == 1 ? "dia" : "dias"}, $hours2 ${hours2 == 1 ? "hora" : "horas"}";
  } else if (years == 0) {
    int days2 = fim.subMonths(months).differenceInDays(inicio);
    return "$months ${months == 1 ? "mês" : "meses"}, $days2 ${days2 == 1 ? "dia" : "dias"}";
  } else {
    int months2 = fim.subYears(years).difference(inicio).inDays ~/ 30;
    return "$years ${years == 1 ? "ano" : "anos"}, $months2 ${months2 == 1 ? "mẽs" : "meses"}";
  }
}

String dateToString(DateTime date, String formato) {
  String format = DateFormat(formato).format(
    date,
  );
  return format;
}

String dateToDbDate(DateTime date) {
  String format = DateFormat("yyyy-MM-ddTHH:mm:ss.mmmuuu").format(date);
  return format;
}

String dbDateToString(String date, String formato) {
  DateTime date2 = dbDateToDate(date);
  String format = DateFormat(formato).format(date2);
  return format;
}

DateTime dbDateToDate(String date) {
  DateTime parse = DateTime.parse(date);
  return parse;
}

Map reverseMap(Map map) {
  Map map2 = Map();
  for (var key in map.keys.toList()) {
    map2[map[key]] = key;
  }
  return map2;
}

nuloOuvazio(List lista) {
  if (lista.isEmpty) {
    return true;
  }
  for (var item in lista) {
    if (item == null) {
      return true;
    } else if (item is String) {
      if (item.trim().isEmpty) {
        return true;
      }
    } else if (item is num) {
      if (item == 0) {
        return true;
      }
    } else if (item is List) {
      if (item.isEmpty) {
        return true;
      }
    }
  }
  return false;
}

encodeMap(Map map) {
  map.forEach((key, item) {
    if (item is DateTime) {
      map[key] = dateToDbDate(item);
    } else if (item is Map) {
      encodeMap(item);
    } else if (item is List) {
      encodeList(item);
    }
  });
}

encodeList(List list) {
  for (var item in list) {
    if (item is Map) {
      encodeMap(item);
    } else if (item is List) {
      encodeList(item);
    }
  }
}

bool isJson(obj) {
  try {
    json.decode(obj);
    return true;
  } catch (_) {
    return false;
  }
}

swapPosition(List list, index1, index2) {
  var item1 = list[index1];
  var item2 = list[index2];
  list.fillRange(index2, index2 + 1, item1);
  list.fillRange(index1, index1 + 1, item2);
}
