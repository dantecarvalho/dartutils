
class SerialAnnotation{
  const SerialAnnotation();
}

abstract class SerialMethods<T> {
  Map<String, dynamic> classToMap();

  Map<String, dynamic> toJson();

  Map<String, dynamic> dbMaptoClassMap(Map original);

  String classToString();

  String listClassToString(List list);

  T stringToClass(String string);

  T mapToClass(Map map);

  List<T> listMapToListClass(List list);

  List<T> listStringToListClass(String listString);

}

