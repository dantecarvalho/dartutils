import 'package:_fe_analyzer_shared/src/base/syntactic_entity.dart';
import 'package:analyzer/dart/analysis/results.dart';
import 'package:analyzer/dart/analysis/session.dart';
import 'package:analyzer/dart/ast/ast.dart';
import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:analyzer/src/dart/element/inheritance_manager3.dart' show InheritanceManager3;
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'serializable.dart';

Builder myBuilder(BuilderOptions opt) => SharedPartBuilder([MyCustomGenerator()], "serial");

class ClassPair {
  ClassElement classElement;
  CompilationUnitMember compilationUnitMember;

  ClassPair(this.classElement, this.compilationUnitMember) {}
}

Future<ClassPair> pairFor(Element element) async {
  AnalysisSession? session = element.session!;
  LibraryElement library = element.library!;
  var resolvedLibraryResult = await session.getResolvedLibraryByElement(library);
  CompilationUnitElement compilationUnitElement = library.definingCompilationUnit;
  resolvedLibraryResult as ResolvedLibraryResult;
  CompilationUnit compilationUnit = resolvedLibraryResult.units.first.unit;
  List<ClassElement> classes = compilationUnitElement.classes;
  NodeList<CompilationUnitMember> declarations = compilationUnit.declarations;
  for (ClassElement classe in classes) {
    if (element == classe) {
      for (CompilationUnitMember declaration in declarations) {
        Element? declaredElement = declaration.declaredElement;
        if (declaredElement == null) {
          continue;
        }
        if (declaredElement == classe) {
          return ClassPair(classe, declaration);
        }
      }
    }
  }
  throw "erro";
}

ClassElement? getSuper(InterfaceType superType, Element element) {
  LibraryElement library = element.library!;
  CompilationUnitElement compilationUnitElement = library.definingCompilationUnit;
  List<ClassElement> classes = compilationUnitElement.classes;
  for (int i = 0; i < classes.length; i++) {
    var classe = classes[i];
    if (classe.thisType == superType) {
      return classe;
    }
  }
  return null;
}

Future<List<FieldDeclaration>> getFields(ClassElement element) async {
  ClassPair classPair = await pairFor(element);

  Iterable<SyntacticEntity> childEntities = classPair.compilationUnitMember.childEntities;
  List<FieldDeclaration> fields = [];
  for (var obj in childEntities) {
    if (obj is FieldDeclaration) {
      fields.add(obj);
    }
  }

  return fields;
}

bool startsWithUppercase(String str) {
  if (str.isEmpty) return false;

  // Use a regular expression to check if the first character is an uppercase letter
  return RegExp(r'^[A-Z]').hasMatch(str);
}

class MyCustomGenerator extends GeneratorForAnnotation<SerialAnnotation> {
  @override
  generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) async {
    print("SerialAnnotation");
    var list = await getFields(element as ClassElement);
    var list2 = await extendFields(element);
    addToMap(Map map, List<FieldDeclaration> list) {
      for (var obj in list) {
        var name = obj.fields.variables.first.name.toString();
        map[name] = obj;
      }
    }

    Map<String, FieldDeclaration> map = {};

    addToMap(map, list2);
    addToMap(map, list);
    String fromJsonFields = textoFromJson(map);

    String fromJson = """
    WWWW _\$WWWWFromJson(Map json) => WWWW()
        $fromJsonFields
    ;

    """;

    String toJsonFields = textoToJson(map);

    String toJson = """
    Map<String, dynamic> _\$WWWWToJson(WWWW instance) => <String, dynamic>{
        $toJsonFields
    };

    """;

    String mix = '''
     mixin _\$Serial {

  Map<String, dynamic> classToMap() {
    return _\$WWWWToJson(this as WWWW);
  }

  Map<String, dynamic> toJson() {
    return _\$WWWWToJson(this as WWWW);
  }
  
  String classToString(){
    return json.encode(this as WWWW);
  }
  
  String listClassToString(List list) {
    return json.encode(list);
  }

  Map<String, dynamic> dbMaptoClassMap(Map original) {
    Map<String, dynamic> map2 = {};
    List allFields = WWWW().reflect().allFields();
    for(var obj in allFields){
      var name = obj.name;
      if (original.containsKey(name)) {
        map2[name] = original[name];
      } else if (original.containsKey(name.toString().toLowerCase())) {
        map2[name] = original[name.toString().toLowerCase()];
      }
    }
    return map2;
  }

 

  WWWW stringToClass(String string) {
    Map map2 = json.decode(string);
    map2 = dbMaptoClassMap(map2);
    return _\$WWWWFromJson(map2);
  }

  WWWW mapToClass(Map map) {
    Map map2 = dbMaptoClassMap(map);
    return _\$WWWWFromJson(map2);
  }

  List<WWWW> listMapToListClass(List list) {
    List<WWWW> list2 = [];
    for(var obj in list){
      list2.add(WWWW.fromJson(obj));
    }
    return list2;
  }
  
  List<WWWW> listStringToListClass(String listString) {
    var list = json.decode(listString);
    return  listMapToListClass(list);
  }

  ClassReflection<WWWW> reflect() {
    return WWWW().reflection;
  }
}

    ''';
    String output = """
      $fromJson
       
      $toJson
       
      $mix
    """;

    var name = element.name;

    return output.replaceAll("WWWW", name);
  }
}

String textoToJson2(TypeAnnotation typeAnnotation, String keyValue) {
  var type = typeAnnotation.toString();
  var dartType = typeAnnotation.type;
  var type2 = typeAnnotation.toString().replaceAll("?", "");

  String question = type.contains("?") ? "?" : "";
  if (startsWithUppercase(type2)) {
    if (type2 == "String") {
      return "$keyValue";
    } else if (type2 == "DateTime") {
      return "$keyValue$question.toIso8601String()";
    } else if (dartType!.element is EnumElement) {
      return "$keyValue$question.name";
    }
    else if (typeAnnotation is NamedType && typeAnnotation.name2.lexeme == "List"){
      var typeArguments = typeAnnotation.typeArguments;
      var arguments = typeArguments?.arguments;
      var first = arguments?.first;
      if (first != null) {
        var texto = textoToJson2(first, "e");
        return "$keyValue.map((e) => $texto).toList()";
      } else {
        return "$keyValue";
      }

    }
    else {
      return "$keyValue$question.toJson()";
    }
  } else {
    return "$keyValue";
  }
}

String textoToJson(Map<String, FieldDeclaration> map){
  String toJsonFields = "";
  for (MapEntry<String, FieldDeclaration> obj in map.entries) {
    var key = obj.key;
    var value = obj.value;
    var typeAnnotation = value.fields.type;

    var texto = textoToJson2(typeAnnotation!, "instance.$key");
    toJsonFields += "'$key': $texto, \n";
  }
  return toJsonFields;
}


String textoFromJson2(TypeAnnotation typeAnnotation, String keyValue) {
  var type = typeAnnotation.toString();
  var dartType = typeAnnotation.type;
  var type2 = typeAnnotation.toString().replaceAll("?", "");
  if (startsWithUppercase(type2)) {
    if (type2 == "String") {
      return "$keyValue as $type";
    } else if (type2 == "DateTime") {
      return "DateTime.parse($keyValue as String)";
    } else if (dartType!.element is EnumElement) {
      return "${type2}.values.byName($keyValue as String)";
    } else if (typeAnnotation is NamedType && typeAnnotation.name2.lexeme == "List") {
      var typeArguments = typeAnnotation.typeArguments;
      var arguments = typeArguments?.arguments;
      var first = arguments?.first;
      if (first != null) {
        var texto = textoFromJson2(first, "e");
        return "($keyValue as List<dynamic>).map((e) => $texto).toList()";
      } else {
        return "($keyValue as List<dynamic>)";
      }
    } else {
      return "${type2}.fromJson($keyValue as Map)";
    }
  } else {
    if (type2 == "int") {
      return "($keyValue as num).toInt()";
    } else if (type2 == "double") {
      return "($keyValue as num).toDouble()";
    } else {
      return "$keyValue as $type";
    }
  }
}


String textoFromJson(Map<String, FieldDeclaration> map) {
  String fromJsonFields = "";
  for (MapEntry<String, FieldDeclaration> obj in map.entries) {
    var key = obj.key;
    var value = obj.value;
    var typeAnnotation = value.fields.type;
    var initializer = value.fields.variables.first.initializer.toString();
    var texto = textoFromJson2(typeAnnotation!, "json['$key']");
    fromJsonFields += "..$key =  json['$key'] != null ? $texto : $initializer \n";
  }
  return fromJsonFields;
}


ClassElement? getClassElement(Element element) {
  if (element is ClassElement) {
    return element;
  }
  if (element.enclosingElement == null) {
    return null;
  }
  return getClassElement(element.enclosingElement!);
}

Future<List<FieldDeclaration>> extendFields(ClassElement element) async {
  var manager = InheritanceManager3();
  var values = manager
      .getInheritedConcreteMap2(element)
      .values;
  Set<ClassElement> elements = Set();
  for (var obj in values) {
    var classElement = getClassElement(obj);
    if (classElement != null) {
      elements.add(classElement);
    }
  }
  var list = elements.toList();
  list.removeAt(0);
  List<FieldDeclaration> fields = [];
  for (ClassElement clasElement in list) {
    var fields2 = await getFields(clasElement);
    fields.addAll(fields2);
  }
  return fields;
}
