import 'package:reflectable/reflectable.dart';
import 'package:shelf_router/shelf_router.dart';
import 'router.dart';

Router getRoutersByClass(List<Type> routeClasses) {
  final router = Router();
  for (Type type in routeClasses) {
    var classMirror = routerAnnotation.reflectType(type) as ClassMirror;
    var newInstance = classMirror.newInstance('', []) as RouterMethods;
    var router2 = newInstance.getRouter();
    router.mount("/", router2.call);
  }
  return router;
}

List<String> noAuthMethods(List<Type> routeClasses) {
  List<String> list = [];
  for (Type type in routeClasses) {
    var classMirror = routerAnnotation.reflectType(type) as ClassMirror;
    var newInstance = classMirror.newInstance('', []) as RouterMethods;
    var reflect = newInstance.reflect();
    var allMethods = reflect.allMethods();
    for (var obj in allMethods) {
      var annotations = obj.annotations;
      var route = getRoute(annotations);
      if (route == null) {
        continue;
      }
      var routerMethodAnnotation = getRouterMethodAnnotation(annotations);
      if (routerMethodAnnotation == null || routerMethodAnnotation.authorization == false) {
        list.add(route.route.replaceFirst("/", ""));
      }
    }
  }
  return list;
}
