import 'dart:async';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:merging_builder/merging_builder.dart';
import 'package:source_gen/source_gen.dart';

import 'router.dart';

Builder myRouterBuilder(BuilderOptions options) {
  final defaultOptions = BuilderOptions({
    'input_files': 'lib/**/*.dart',
    'output_file': 'lib/routers.g.dart',
    'sort_assets': true,
  });

  // Apply user set options.
  options = defaultOptions.overrideWith(options);
  return MergingBuilder<String, LibDir>(
    generator: MyCustomRouterGenerator(),
    inputFiles: options.config['input_files'],
    outputFile: options.config['output_file'],
    sortAssets: options.config['sort_assets'],
  );
}

class MyCustomRouterGenerator extends MergingGenerator<String, RouterAnnotation> {
  @override
  String generateStreamItemForAnnotatedElement(
    Element element,
    ConstantReader annotation,
    BuildStep buildStep,
  ) {
    print("RouterAnnotation");
    String s = "import '" + element.library!.identifier + "';";
    return s;
  }

  @override
  FutureOr<String> generateMergedContent(Stream<String> stream) async {
    print("RouterAnnotation 2");
    String s = "";
    await for (final names in stream) {
      s += names + "\n";
    }

    return s;
  }
}
