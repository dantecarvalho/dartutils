import 'package:reflectable/reflectable.dart';
import 'package:reflection_factory/reflection_factory.dart';
import 'package:shelf_router/shelf_router.dart';

class RouterAnnotation extends Reflectable {
  const RouterAnnotation() : super(reflectedTypeCapability, newInstanceCapability);
}

const routerAnnotation = RouterAnnotation();

class RouterMethodAnnotation {
  final bool authorization;

  const RouterMethodAnnotation({
    this.authorization = false,
  });
}

RouterMethodAnnotation? getRouterMethodAnnotation(List<Object> anotacoes) {
  for (var obj in anotacoes) {
    if (obj is RouterMethodAnnotation) {
      return obj;
    }
  }
  return null;
}

Route? getRoute(List<Object> anotacoes) {
  for (var obj in anotacoes) {
    if (obj is Route) {
      return obj;
    }
  }
  return null;
}

abstract class RouterMethods<T> {
  Router getRouter();

  ClassReflection<T> reflect();
}
